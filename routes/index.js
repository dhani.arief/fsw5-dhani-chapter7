var express = require('express');
var router = express.Router();

const authCtrl = require('../controllers/auth.controller')
const homeCtrl = require('../controllers/home.controller')

const middleware = require('../middleware/restrict')

/* GET home page. */
router.get('/',middleware.restrict ,homeCtrl.showIndexPage)

//Register Page
router.get('/register', authCtrl.showRegisterPage)
router.post('/register', authCtrl.register)

//Login Page
router.get('/login', authCtrl.showLoginPage)
router.post('/login', authCtrl.login)

//Whoami Page
router.get('/whoami', middleware.restrict, authCtrl.whoami)

//Logout
// router.post('/logout', middleware.restrict, authCtrl.logout)

module.exports = router;
