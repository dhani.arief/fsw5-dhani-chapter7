'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userGameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  userGameHistory.init({
    username: DataTypes.STRING,
    score: DataTypes.STRING,
    playdate: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'userGameHistory',
  });
  return userGameHistory;
};