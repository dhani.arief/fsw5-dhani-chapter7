const { userGameHistory } = require("../models")

const score = (req, res, next) =>{
    userGameHistory.score(req.body)
    .then(()=>{
        res.redirect('/a')
    })
    .catch((error)=>next(error.message))
}

module.exports = {
    score
}