const { User } = require("../models");


const register = (req, res, next) =>{
    User.register(req.body)
        .then(()=>{
            res.redirect('/login')
        })
        .catch((error)=>(error.message))
}

const showRegisterPage = (req,res)=>{
    res.render('register')
};

const showLoginPage = (req,res)=>{
    res.render('Login')
};

const login = (req,res,next)=>{
    return passport.authenticate('local',{
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true,
    successFlash: true
    })(req,res,next)
};

const whoami=(req,res)=>{
    res.render('profile', req.user.dataValues)
};

const logout = (req,res) =>{
    req.logout();
    res.redirect('/login')
};

module.exports = {
    register,
    showRegisterPage,
    login,
    showLoginPage,
    whoami,
}